<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\Models\Shift;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ShiftTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed');
    }

    /**
     * Test the index method of the ShiftController.
     */
    public function testIndex()
    {
        $user = User::factory()->create();

        $shifts = Shift::factory()->count(10)->create();

        $response = $this->actingAs($user)
                     ->getJson('/api/V1/shifts');

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'data'
        ]);
    }

    /**
     * Test the store method of the ShiftController.
     */
    public function testStore()
    {
        $shiftData = Shift::factory()->make([
            'date' => Carbon::now()->format('Y-m-d'),
            'start_time' => Carbon::now()->addHours(1)->format('H:i')
        ])->toArray();

        $response = $this->actingAs($user = User::factory()->create(), 'sanctum')
                         ->postJson('/api/V1/shifts', $shiftData);

        $response->assertCreated();
        $response->assertJsonStructure([
            'status',
            'data'
        ]);
    }

    /**
     * Test the show method of the ShiftController.
     */
    public function testShow()
    {
        $shift = Shift::factory()->create();

        $response = $this->actingAs($user = User::factory()->create(), 'sanctum')
                         ->getJson("/api/V1/shifts/{$shift->id}");

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'data'
        ]);
    }

    /**
     * Test the update method of the ShiftController.
     */
    public function testUpdate()
    {
        $shift = Shift::factory()->create();

        $updateData = [
            'start_time' => Carbon::now()->addHours(2)->format('H:i'),
            'date' => Carbon::now()->format('Y-m-d')
        ];

        $response = $this->actingAs($user = User::factory()->create(), 'sanctum')
                         ->putJson("/api/V1/shifts/{$shift->id}", $updateData);

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'message'
        ]);
    }

    /**
     * Test the destroy method of the ShiftController.
     */
    public function testDestroy()
    {
        $shift = Shift::factory()->create();

        $response = $this->actingAs($user = User::factory()->create(), 'sanctum')
                         ->deleteJson("/api/V1/shifts/{$shift->id}");

        $response->assertOk();
        $response->assertJsonStructure([
            'message'
        ]);
    }
}
