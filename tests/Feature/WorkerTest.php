<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\Models\User;
use App\Models\Worker;
use Illuminate\Database\Eloquent\Factories\Factory;


class WorkerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed');
    }

    /**
     * Test the index method of the WorkerController.
     */
    public function testIndex()
    {
        $user = User::factory()->create();

        $workers = Worker::factory()->count(10)->create();

        $response = $this->actingAs($user)
                     ->getJson('/api/V1/workers');

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'data'
        ]);
    }

    /**
     * Test the store method of the WorkerController.
     */
    public function testStore()
    {
        $workerData = [
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ];

        $response = $this->actingAs($user = User::factory()->create(), 'sanctum')
                         ->postJson('/api/V1/workers', $workerData);

        $response->assertCreated();
        $response->assertJsonStructure([
            'status',
            'message'
        ]);
    }

    /**
     * Test the show method of the WorkerController.
     */
    public function testShow()
    {
        $worker = Worker::factory()->create();

        $response = $this->actingAs($user = User::factory()->create(), 'sanctum')
                         ->getJson("/api/V1/workers/{$worker->id}");

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'data'
        ]);
    }

    /**
     * Test the update method of the WorkerController.
     */
    public function testUpdate()
    {
        $worker = Worker::factory()->create();

        $updateData = [
            'name' => 'Jane Doe',
            'email' => 'janedoe@example.com',
            'password' => 'password'
        ];

        $response = $this->actingAs($user = User::factory()->create(), 'sanctum')
                         ->putJson("/api/V1/workers/{$worker->id}", $updateData);

        $response->assertOk();
        $response->assertJsonStructure([
            'status',
            'message'
        ]);
    }

    /**
     * Test the destroy method of the WorkerController.
     */
    public function testDestroy()
    {
        $worker = Worker::factory()->create();

        $response = $this->actingAs($user = User::factory()->create(), 'sanctum')
                         ->deleteJson("/api/V1/workers/{$worker->id}");

        $response->assertOk();
        $response->assertJsonStructure([
            'message'
        ]);
    }
}
