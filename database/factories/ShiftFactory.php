<?php

namespace Database\Factories;

use App\Models\Shift;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ShiftFactory extends Factory
{
    protected $model = Shift::class;

    public function definition()
    {
        return [
            'worker_id' => User::factory(),
            'date' => Carbon::now()->addDays($this->faker->numberBetween(1, 7))->format('Y-m-d'),
            'start_time' => Carbon::now()->addHours($this->faker->numberBetween(0, 23))->format('H:i'),
            'end_time' => Carbon::now()->addHours($this->faker->numberBetween(1, 8))->format('H:i'),
        ];
    }
}
