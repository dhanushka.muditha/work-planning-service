<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\ShiftRepositoryInterface;
use App\Repositories\WorkerRepositoryInterface;
use App\Repositories\WorkerRepository;
use App\Repositories\ShiftRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(ShiftRepositoryInterface::class, ShiftRepository::class);
        $this->app->bind(WorkerRepositoryInterface::class, WorkerRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
