<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shift;
use Carbon\Carbon;
use App\Repositories\ShiftRepositoryInterface;

class ShiftController extends Controller
{
    protected $shiftRepository;

    public function __construct(ShiftRepositoryInterface $shiftRepository)
    {
        $this->shiftRepository = $shiftRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $shift =  $this->shiftRepository->all();

        if ($shift->isEmpty()) {
            return response()->json([
                'status' => 'error',
                'message' => 'No Shifts Found'
            ]);
        }else {
            return response()->json([
                'status' => 'success',
                'data' => $shift
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'worker_id' => 'required',
            'start_time' => 'required|date_format:H:i',
            'date' => 'required|date_format:Y-m-d'
        ]);
        
        $shiftCreate = $this->shiftRepository->createShift($request);

        if (!$shiftCreate) {
            return response()->json([
                'status' => 'error',
                'message' => 'Shift creation failed'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'data' => $shiftCreate
        ], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $shift = $this->shiftRepository->getShiftInfo($id);

        if (!$shift) {
            return response()->json([
                'message' => 'Shift not found'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'data' => $shift
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'start_time' => 'required',
            'date' => 'required'
        ]);


        $updateShift = $this->shiftRepository->updateShift($id, $request);

        if ($updateShift) {
            return response()->json([
                'status'=> 'error',
                'message' => 'No changes found'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Data Table Updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $removeshift = $this->shiftRepository->destroy($id);

        if (!$removeshift) {
            return response()->json([
                'message' => 'Data not Available'
            ], 404);
        }

        return response()->json([
            'message' => 'Shift deleted'
        ]);
    }
}
