<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Repositories\WorkerRepositoryInterface;

class WorkerController extends Controller
{
    protected $workerRepository;

    public function __construct(WorkerRepositoryInterface $workerRepository)
    {
        $this->workerRepository = $workerRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $workers =  $this->workerRepository->all();

        if (!$workers) {
            return response()->json([
                'status' => 'error',
                'message' => 'No Workers Found'
            ]);
        }
        return response()->json([
            'status' => 'success',
            'data' => $workers
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:55',
            'email' => 'email|required|unique:workers',
            'password' => 'required|confirmed'
        ]);

        $worker = $this->workerRepository->create($validatedData);

        if (!$worker) {
            return response()->json([
                'status' => 'error',
                'message' => 'User registration failed'
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'User registration successful. Thank you for registering with us!'
        ], 201);

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $findWorker = $this->workerRepository->show($id);

        if (!$findWorker) {
            return response()->json([
                'status'=> 'error',
                'message' => 'Worker not find'
            ]);
        }

        return response()->json([
            'status'=> 'success',
            'data' => $findWorker
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $updateWorker = $this->workerRepository->update($id, $request);

        if (!$updateWorker) {
            return response()->json([
                'status'=> 'error',
                'message' => 'No changes found'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Data Table Updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $deleteWorker = $this->workerRepository->destroy($id);

        if (!$deleteWorker) {
            return response()->json([
                'message' => 'Worker not found'
            ], 404);
        }

        return response()->json([
            'message' => 'Worker deleted'
        ]);
    }
}
