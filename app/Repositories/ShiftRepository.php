<?php

namespace App\Repositories;

use App\Models\Shift;
use App\Models\Worker;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Hash;


class ShiftRepository implements ShiftRepositoryInterface
{
    public function all()
    {
        return Shift::all();
    }

    public function findShiftInfo($id)
    {
        return Shift::where('worker_id', $id)
            ->first();
    }

    public function isWorkerAvailable($workerId, $date, $startTime, $endTime)
    {
        $shifts = Shift::onDate($date)
            ->inTimeRange($startTime, $endTime)
            ->forWorker($workerId)
            ->get();

        return $shifts->isEmpty();
    }

    public function createShift($request)
    {
        $endTime = Carbon::parse($request['start_time'])->addHours(8);

        if (!self::isWorkerAvailable($request['worker_id'], $request['date'], $request['start_time'], $endTime)) {
            return ('Worker already has a shift on the same day');
        }

        return Shift::create([
            'worker_id' => $request['worker_id'],
            'start_time' => $request['start_time'],
            'end_time' => $endTime,
            'date' => $request['date']
        ]);
    }

    public function getShiftInfo($id)
    {
        $findShift = Shift::where('worker_id', $id)->get();

        if (sizeof($findShift)) {
            return $findShift;
        }

        return false;
    }

    public function checkISthereAnyShiftForWorker($date, $startTime, $id)
    {
        $availableShifts = Shift::onDate($date)
            ->forWorker($id)
            ->forStartTime($startTime)
            ->get();

            return $availableShifts;
    }

    public function updateShift($id, $request)
    {
        $findShiftAssign = self::checkISthereAnyShiftForWorker($request['date'], $request['start_time'], $id);

        if (sizeof($findShiftAssign)) {
            $shiftUpdate = self::findShiftInfo($id);
            $shiftUpdate->worker_id = $id;
            $shiftUpdate->start_time = $request['start_time'];
            $shiftUpdate->date = $request['date'];
            $shiftUpdate->save();

            $changes = $shiftUpdate->getChanges();
            return ($changes) ? true : false;
        }

        return false;
    }

    public function destroy($id)
    {
        $deleteWorker = self::findShiftInfo($id);

        if ($deleteWorker) {
            return $deleteWorker->delete();
        }

        return false;
    }
}
