<?php

namespace App\Repositories;

use App\Models\Shift;
use Illuminate\Support\Collection;

interface ShiftRepositoryInterface
{
    public function all();
    public function isWorkerAvailable($workerId, $date, $startTime, $endTime);
    public function createShift($request);
    public function getShiftInfo($id);
    public function checkISthereAnyShiftForWorker($date, $startTime, $id);
    public function updateShift($id, $request);
    public function destroy($id);
}
