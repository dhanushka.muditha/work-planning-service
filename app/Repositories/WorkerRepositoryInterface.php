<?php

namespace App\Repositories;

interface WorkerRepositoryInterface
{
    public function all();
    public function create($request);
    public function show($id);
    public function update($findWorker, $request);
    public function destroy($id);
}
