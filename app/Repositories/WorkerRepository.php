<?php

namespace App\Repositories;

use App\Models\Worker;
use Illuminate\Support\Facades\Hash;

class WorkerRepository implements WorkerRepositoryInterface
{
    public function all()
    {
        return Worker::all();
    }

    public function create($validatedData): Worker
    {
        $worker = new Worker();

        $worker->create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'password' => $validatedData['password']
        ]);

        return $worker;
    }

    public function show($id)
    {
        return Worker::find($id);
    }

    public function update($id, $request)
    {
        $findWorker = self::show($id);

        // Check if the password is set and verify it
        if (isset($request->password)) {
            $passWord = Hash::check($request->password, $findWorker->password);
            if (!$passWord) {
                $findWorker->password = Hash::make($request->password);
            }
        }

        // Update the name and email
        $findWorker->name = $request->name;
        $findWorker->email = $request->email;

        // Save the changes to the database
        $findWorker->save();
        $changes = $findWorker->getChanges();

        return ($changes) ? true : false;
    }


    public function destroy($id)
    {
        $deleteWorker = self::show($id);
        if ($deleteWorker) {
            return $deleteWorker->delete();
        }

        return false;
    }
}
